package drones;

import java.util.Random;
import java.io.Serializable;
import drones.Direction.direction;
import java.io.Serializable;
import java.util.ArrayList;

public class DroneArena implements Serializable {

    public int droneArenaLength, droneArenaWidth; // declare variables to hold the size of the drone arena
    private int xpos, ypos;
    Random randomGenerator = new Random(); // declare a randomGenerator
    protected ArrayList<Drone> droneList; // declare arrayList of type Drone which will hold drone objects
    static String sv = " "; // initialize string to null

    /**
     * The constructor takes two variables which is the desired size of the arena.
     * The constructor also initialises the ArrayList to be called droneList
     * 
     * @param l
     * @param w
     */

    public DroneArena(int l, int w) { // constructor

        droneArenaLength = l; // initialise input to value
        droneArenaWidth = w; // initialise input to value
        droneList = new ArrayList<Drone>();// initialise list of type drone

    }

    /**
     * The addDrone() method simply uses the random generatot to assign an x and a y
     * value to the drone that is within the constructor input constraints. The
     * addDrone method then creates a new drone object, with an x and y value, and
     * then adds it to the ArrayList.
     */

    public void addDrone() {

        xpos = randomGenerator.nextInt(droneArenaLength) + 1; // xpos = random int under lenth of canvas
        ypos = randomGenerator.nextInt(droneArenaWidth) + 1; // ypos = random int under width of canvas
        if(getDroneAt(xpos, ypos)){
        droneList.add(new Drone(xpos, ypos, direction.NORTH.toString()));// adds Drone object to DroneList
        }

    }

    /**
     * This function returns information on the drone objects in the ArrayList. It
     * uses an enhance for loop to iterate through all the Drones, for each drone,
     * it adds its toString output to a string names sval. Once it has iterated
     * through all of the Strings, it then returns the droneArena sizes, plus the
     * string of information on reach drone in the dronelist.
     * 
     * @return Stirng
     */

    public String toString() {

        String sval = "";

        for (Drone i : droneList) {

            sval += i.toString() + "\n";
        }

        return ("Arena Length = " + droneArenaLength + "\n" + "Arena Width = " + droneArenaWidth + "\n" + sval + "\n");
    }

    /**
     * Show all the drones in the interface
     * 
     * @param c1 the canvas in which drones are shown
     */

    public void showDrones(ConsoleCanvas c1) {

        for (Drone i : droneList) { // for drones in the droneList
            i.displayDrone(c1); // for each drone object in the droneList, displayDrone.

        }

    }

    /**
     * This method gets the Drone
     * @param x
     * @param y
     * @return
     */

    public boolean getDroneAt(int x, int y) {

        for (Drone i : droneList) {
            if (i.xpos == x && i.ypos == y) {
                return false;
            }
        }

        return true;

    }

    /**
     * returns false if the x,y position passes is outside of the arena, otherwise
     * it returns wethere there is a drone there
     * 
     * @return
     */

    public boolean canMoveHere(int xp, int xy) { 

        if (xp > 0 && xp < droneArenaLength + 1) { // nested if consition
            if (xy > 0 && xy < droneArenaWidth + 1) {
                for (Drone i : droneList){
                    if(xp == i.xpos && xy == i.ypos){
                        
                    }

                }

                return true;
            }

        }
        return false;

    }

    /**
     * for each drone, trytoMove, and if this is possible, move the Drone
     * @param myArena
     */

    public void moveAllDrones(DroneArena myArena) {

        // for all drones
        // tryToMove

        for (Drone i : droneList) {
            i.tryToMove(i, myArena);
        }
        System.out.print("\033[H\033[2J");
        System.out.flush();

    }


    /**
     * This is the Class's Main Method
     * @param args
     */

    public static void main(String[] args) {

        // DroneArena arena = new DroneArena(10, 10); // creates new object and
        // constructs the size of arena
        // arena.addDrone(); // adds drone to the middle of the arena
        // arena.addDrone();
        // arena.addDrone();
        // for (Drone i : droneList) {
        // sv += i.toString() + "\n";
        // }
        // System.out.println(sv);

        // }

    }
}