
import drones.ConsoleCanvas;
import drones.Drone;
import drones.DroneArena;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFileChooser;


public class DroneInterface { // essentially the driver class

    private Scanner scn; // declares a scanner object
    public DroneArena myArena; // declares a DroneArena object
    private int x, y; // declares two private Integer variables
    Scanner obj = new Scanner(System.in);
    protected String mystring;

    /**
     * This constructor essentilly displays an interface in which the user can
     * decide how they would like to manipulate the program.
     */

    void doDisplay() {

        ConsoleCanvas c1 = new ConsoleCanvas(x, y); // create suitably sized ConsoleCanvas object
        myArena.showDrones(c1); // call showDrones method, passing the ConsoleCanvas object just create
        System.out.println(c1.toString()); // prints return value of overriden toString function which coverts array to
                                           // string
    }

    /**
     * Constructor that allows for user interaction
     */

    public DroneInterface() {

        x = 15; // default values of arena
        y = 15; // default values of arena 

        scn = new Scanner(System.in); // declare Scanner object
        myArena = new DroneArena(x, y); // declare arena object

        char input = ' '; // initialize char to Null

        
        do {
            System.out.println(
                    "Enter 'A' to add drone || Enter 'I' to get Information ||\nEnter 'D' to display Drones || Enter 'M' to move the Drones ONCE ||\nEnter 't' to move the Drones 10 times || Save DroneArena 'S' ||\nLoad DroneArena 'L' || Enter 'q' to exit\n");
            input = scn.next().charAt(0);
            switch (input) {

            case 'A':
            case 'a':
                myArena.addDrone(); // addDrone to droneList

                break;
            case 'I':
            case 'i':
                System.out.print(myArena.toString()); // call Drone toString() method
                break;
            case 'D':
            case 'd':
                doDisplay(); // display the arena.

                break;
            case 'M':
            case 'm':

                myArena.moveAllDrones(myArena); // move drones once
                doDisplay(); // display the results

                break;

            case 'S':
            case 's':

                JButton open = new JButton(); // declaring and initializing JFileChooser
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(new java.io.File("/files"));
                fc.setDialogTitle("Choose File");
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                if (fc.showOpenDialog(open) == JFileChooser.APPROVE_OPTION) { // if dialogBox open
                }

                try (FileOutputStream fileout = new FileOutputStream(fc.getSelectedFile())) { // create an Output Stream
                        
                        ObjectOutputStream os = new ObjectOutputStream(fileout);
                        os.writeObject(myArena);
                        mystring = fc.getSelectedFile().toString();

                    os.close();

                } catch (Exception e) { // catch any exceptions
                }

                break;

            case 'L':
            case 'l':

                try (FileInputStream filein = new FileInputStream(mystring)) { // create an Input Stream

                    ObjectInputStream os = new ObjectInputStream(filein);
                    DroneArena tempObj = (DroneArena) os.readObject();
                    myArena = tempObj;
                    x = tempObj.droneArenaLength; // change values in ConsoleCanvas
                    y = tempObj.droneArenaWidth;

                } catch (Exception e) { // catch any exceptions

                    System.out.println(e); // print exceptions to console
                }

                break;

            case 'T':
            case 't':
                
            // move drones 10 times

                for (int i = 0; i < 10; i++) {
                    myArena.moveAllDrones(myArena);
                    doDisplay();

                    try {
                        Thread.sleep(100); // delay refresh

                    } catch (InterruptedException e) {
                    }

                }

                break;

            case 'N':
            case 'n':

                // create new arena based on UserInput

                System.out.print("X val --> ");
                Integer xinput = obj.nextInt();
                System.out.print("Y val --> ");
                Integer yinput = obj.nextInt();
                myArena = new DroneArena(xinput, yinput);
                x = xinput;
                y = yinput;

                break;
            
            case 'q':

                //quit operations

                break;
            }
        } while (input != ('q'));

        scn.close();

    }

    /**
     * Main Method
     * @param args
     */

    public static void main(String[] args) { //
        DroneInterface r = new DroneInterface();// create an object of type DroneInterface and call the constructor

    }

}