
package drones;

import java.util.Random;

/**
 * Class that will encapsulate the attributes and methods 
 * associated with direction
 */

public class Direction {

    static int directionCounter = -1;

    /**
     * declare enum class
     */

    public enum direction { 

        NORTH, SOUTH, EAST, WEST;
    }

    /**
     * Method will return a randomm direction
     * @return
     */
    public static direction randomDirection() {

        return direction.values()[new Random().nextInt(direction.values().length)];

    }

    /**
     * Method will return a consequential direction
     * @return
     */

    public static direction nextDirection() {

        if (directionCounter == 3) { // loops the directions
            directionCounter = -1;
        }
        directionCounter++;

        return direction.values()[directionCounter];

    }

    public static void main(String[] args) {

        System.out.println(nextDirection());
        System.out.println(randomDirection());

    }

}