package drones;

import java.util.StringJoiner;
import java.io.Serializable;
import java.util.Arrays;

public class ConsoleCanvas implements Serializable { // class that implements Serializable

        
        private static final long serialVersionUID = 1L;
        public char[][] canvasArray; // declaring array
        protected int x, z, dxPos, dyPos; // global class variable

        /**
         * Console Canvas class essentially enables you to input the size of your
         * droneArena the constructor wil then make an array based on the size. name of
         * array is canvasArray[]
         * 
         * @param a
         * @param k
         */

        public ConsoleCanvas(int a, int k) { // creates an array of a/k dimensions

                x = a + 2;
                z = k + 2;

                canvasArray = new char[x][z];
                for (int j = 0; j < z; j++) {
                        for (int i = 0; i < x; i++) {
                                if (j == 0) {
                                        canvasArray[i][j] = '#';

                                }

                                else if (i == z - z) {
                                        canvasArray[i][j] = '#';
                                } else if (i == z - 1) {
                                        canvasArray[i][j] = '#';

                                } else if (j == x - 1) {
                                        canvasArray[i][j] = '#';

                                } else {

                                        canvasArray[i][j] = ' ';
                                }

                        }

                } // whole for loop adds hash tags to exterior of canvas

        }

        /**
         * the show it method allows you to feed the position at which you want your
         * drone in the array, and the letter you want to be represented in the array.
         * 
         * @param ab
         * @param ac
         * @param drnD
         * @return void
         */

        public void showIt(int ab, int ac, char drnD) { // adds values to canvas-array, and displays

                canvasArray[ab][ac] = drnD; // puts drone at assigned location
                dxPos = ab;
                dyPos = ac;
        }

       
        /**
         * The overriden toString method displays the Canvas 
         */

        public String toString() {

                // return ("\nD has been added to postion X --> " + dxPos + " and position Y -->
                // " + dyPos);
                StringJoiner sj = new StringJoiner(System.lineSeparator());
                for (char[] row : canvasArray) { // for each row
                        sj.add(Arrays.toString(row));
                }
                String result = sj.toString();
                return result;
        }

        /**
         * the Main Method
         * @param args
         */

        public static void main(String[] args) {

                ConsoleCanvas cvs = new ConsoleCanvas(15, 15); // create object of type consolecanvas, call constructor
                cvs.showIt(3, 10, 'D'); //
                System.out.println(cvs.toString()); // displays result of showIt method
        }

}
