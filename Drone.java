
package drones;

import java.io.Console;
import java.io.Serializable;
import drones.Direction.direction;
import java.util.Random;

/**
 * this class Drone will hold the Methods and Attributes associated with 
 * a Drone object
 */


public class Drone implements Serializable {
    
    Random randomGenerator = new Random();
    protected int xpos, ypos; // declaring x and y variables
    protected String direc;
     Integer droneId;
    

    /**
     * This constructor initialises an x and y value for each drone object.
     * It also initialises a 
     * 
     * @param x
     * @param y
     */

    public Drone(int x, int y, String z) { // constructor

        xpos = x; // initializes constructor input to xpos
        ypos = y; // initialises constructor input to ypos
        direc = z; // initializes constructor input to z
        droneId = randomGenerator.nextInt(2000);

    }  

    /**
     * this overriden function simply returns information that has been gathered via
     * the constructor
     */

    public String toString() { // overiding the standard toString() method in util class

        return ("The drone is at x: " + xpos + " y: " + ypos + ", " + "Direction of Drone is " + direc + " :: DroneID " + droneId  );

    }

    /**
     * The display drone method simply calls the showIt medthod which in turn displays the drone
     * @param c1
     */

    public void displayDrone(ConsoleCanvas c1) { // takes a ConsoleCanvas Object

        c1.showIt(xpos, ypos, 'D'); // call the showIt method in c to put a D where the drone is

    }

    

    /**
     * calculates the new position, calls canMoveHere and puts the drone there
     */

    
     /**
      * This method tests if a singular Dron object can move to a designated coordinate.
      * @param b
      * @param c
      */
    
     public void tryToMove(Drone b, DroneArena c) { // for a singular drone

        int tempx = b.xpos; // assigns temporary value to drone x position
        int tempy = b.ypos; // assigns temporary value to drone y position

        //String s = Direction.randomDirection().toString();

        
    

        if (c.canMoveHere(tempx - 1, tempy) && b.direc == "NORTH") { // if the dron can move north, move North

            b.xpos = tempx - 1; // re-initialize drone coordinate

        } else if (c.canMoveHere(tempx, tempy + 1) && b.direc == "NORTH") { // if the drone can move EAST, move east

            b.ypos = tempy + 1; // re-initialize drone coordinate

            b.direc = "EAST";
        }

        else if (c.canMoveHere(tempx + 1, tempy) && b.direc == "EAST") { // if Drone can move SOUTH, move south
            
            b.xpos = tempx + 1; // re-initialize drone coordinate

        } else if (c.canMoveHere(tempx, tempy - 1) && b.direc == "EAST") { // if Drone can move WEST, move west

            b.ypos = tempy - 1; // re-initialize drone coordinate
            b.direc = "NORTH";
        }
    }




    

        // switch(s) {

        // case "NORTH":

        // tempx = b.xpos;
        // tempy = b.ypos-1;

        // break;

        // case "EAST":

        // tempx = b.xpos;
        // tempy = b.ypos+1;

        // break;

        // case "SOUTH":

        // tempx = b.xpos+1;
        // tempy = b.ypos;

        // break;

        // case "WEST":

        // tempx = b.xpos-1;
        // tempy = b.ypos;

        // break;

        // default:

        // tempx = b.xpos;
        // tempy = b.ypos;

        // break;

        // }

        // if (c.canMoveHere(tempx, tempy) == true) {

        // b.xpos = tempx;
        // b.ypos = tempy;

        // }

    
/**
 * this is the main function
 * @param args
 */
    
    public static void main(String[] args) {

        Drone mydrone = new Drone(3, 4, direction.NORTH.toString());
        System.out.println(mydrone.toString());

    }

}